package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class StartFragment extends Fragment {

    Button login, register;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);
        login = view.findViewById(R.id.log_in_ma);
        register = view.findViewById(R.id.register_ma);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().
                        beginTransaction().
                        setCustomAnimations(R.animator.slide_in_left, R.animator.slide_in_right).
                        replace(R.id.main_activity, new LogIn()).
                        addToBackStack(null).commit();
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().
                        beginTransaction().
                        setCustomAnimations(R.animator.slide_in_left, R.animator.slide_in_right).
                        replace(R.id.main_activity, new Registration()).
                        addToBackStack(null).commit();
            }
        });
        return view;
    }
}